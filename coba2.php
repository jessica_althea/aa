<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css"
        integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
    <title>P WEB</title>
    <link rel="stylesheet" href="jasweb.css"> 
    <!-- kl g konek tambahin ini aja -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"
        integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJek<link rel="stylesheet" href="coba2.css">YhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous">
    </script>
</head>

<body>
    <!--NAVBAR-->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
            <img src="bebas/Logo.svg" alt="">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
             <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
                 <li class="nav-item active">
                   <a class="nav-link" href="#">Home </a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="#">Service</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="#">Courses</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#">About Us</a>
                 </li>
                 <li class="nav-item">
                       <a class="nav-link" href="#">Contact Us</a>
                 </li>
                 <button class="btn btn-primary">Log In</button>
               </ul>
             </div>
            </div>
           </nav>
    <!--Ahkir Navbar-->  
    
<!-- Jumbotron-->
<div class="jumbotron jumbotron-fluid d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col">
          <div class="col-lg-9">
            <h1 class="display-4">Get Access to Unlimited Educational Resources. Everywhere, Everytime!</h1>
            <p class="lead">premiun access to more than 10,000 resources rating from courses, events e.t.c.</p>
            <button class="btn btn-primary">Get Access</button>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Akhir Jumbotron-->

<!-- Service -->
<section class="services d-flex align-items-center justify-content-center" id="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mb-5">
        <h4>Service</h4>
      </div>
    </div>
    <div class="row">  
      <div class="col-lg-4">
        <img src="bebas/service1.svg" alt="">
        <h6>Unlimited Access</h6>
        <p>One subscription unlimited access</p>
      </div>
      <div class="col-lg-4">
        <img src="bebas/service2.svg" alt="">
        <h6>Expert Teachers</h6>
        <p>Learn from industry experst who are passionate about teaching</p>
      </div>
      <div class="col-lg-4">
        <img src="bebas/service3.svg" alt="">
        <h6>Learn Anywhere</h6>
        <p>Switch between computer, tablet, or mobile device.</p>
      </div>
    </div>
  </div>
</section>
<!-- Akhir Service -->

<!-- Stories -->
<section class="stories d-flex align-items-center" id="stories">
  <div class="container">
    <div class="row">
      <div class="col-lg-5">
        <h1>Success Stories From Our Student WorldWide!</h1>
        <p>Semaj Africa is an online education platform that delivers video courses, program and resources for Individual, Advertising & Media Specialist. Online Marketing Professionals, Freelancers and anyone looking to pursue a career in digital marketing, Accounting, Web development, Programing, Multimedia and CAD design.</p>
        <button class="btn btn-primary">Discover</button>
      </div>
      <div class="col-lg-7 text-center">
        <img src="bebas/stories-img.png" alt="" width="80%">
      </div>
    </div>
  </div>
</section>
<!-- Akhir Stories -->

<!-- Courses -->
<section class="courses d-flex align-items-center" id="courses">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
      <!-- Baris Pertama -->
        <h4>Courses</h4>
      </div>
      <!-- Baris kedua/linknya -->
      <div class="row pb-4">
        <div class="col-lg-12 text-center">
          <a href="">All</a>
          <a href="">Design</a>
          <a href="">Web Development</a>
          <a href="">Digital</a>
          <a href="">Photography</a>
          <a href="">Motion Graphics</a>
          <a href="">Digital Marketing</a>
        </div>
      </div> 
      <!-- Baris ketiga/baris pertama card -->
      <div class="row">
      
      <?php
      include "koneksi.php";
      $data = mysqli_query($koneksi, "select*from courses");
      while($d = mysqli_fetch_array($data)){
      ?>
      <div class="col-lg-3">
          <div class="card">
            <div class="card-top text-center">
              <?php echo $d['princing'] ?>
            </div>
            <img src="bebas/<?php echo $d['img']?>" class="card-img-top" alt="...">
            <div class="card-body">
              <p><?php echo $d['description']?></p>
            </div>
          </div>
      </div>

      <?php
      }
      ?>
      </div>
      <!-- ini baris keenam -->
      <div class="row justify-content-center mt-5">
        <button class="btn btn-primary">Discover</button>
      </div>
    </div>
</section>
<!-- Akhir Courses -->

<!-- Achievement -->
<section class="achievement d-flex align-items-center" id="achievement">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <img src="bebas/Achievement1.svg" alt="">
        <h1>5,679</h1>
        <p class="o">Registered Student</p>
      </div>
      <div class="col-lg-3">
        <img src="bebas/Achievement2.svg" alt="">
        <h1>2,679</h1>
        <p class="o">Student has been helped to achieve their dreams</p>
      </div>
      <div class="col-lg-3">
        <img src="bebas/Achievement3.svg" alt="">
        <h1>10,000</h1>
        <p class="j">More than 10,000 people visits our site monthly</p>
      </div>
      <div class="col-lg-3">
        <img src="bebas/Achievement4.svg" alt="">
        <h1>#10</h1>
        <p class="o">Ranked among the top 10 growing online learning startups in West Africa</p>
      </div>
    </div>
  </div>
</section>
<!-- Akhir Achievetment -->

<!-- Testimonials -->
<section class="testimonials d-flex align-items-center id="testimonials>
<div class="container-fluid">
  <div class="row text-center">
    <div class="col-lg-12">
      <h1>What Student Say</h1>
    </div>
  </div>
  <div class="row text-center mb-3 justify-content-center">
    <div class="col-lg-8">
      <p>Semaj Africa is an online education platfrom that delivers video courses, program and resources for Individual, Advertising & Media Specialist,.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3">
      <div class="card">
        <div class="container">
            <img src="bebas/testi.svg" alt=""  >
            <p class="card-text pt-3 pb-3">Semaj Africa is an online education platfrom that delivers video courses, program and resources for Individual, Advertising & Media Specialist, Online Marketing professional, Freelancers and anyone.</p>
            <div class="row">
              <div class="col-lg-3">
                <img src="bebas/User-Pic.svg" alt="">
              </div>
                <div class="col-lg-8 pt-2 ml-2">
                  <h5>Arthur Broklyn</h5>
                  <p>Categories: 3d Modelling</p>
                </div>      
            </div>
          </div>
        </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="container">
            <img src="bebas/testi.svg" alt=""  >
            <p class="card-text pt-3 pb-3">Semaj Africa is an online education platfrom that delivers video courses, program and resources for Individual, Advertising & Media Specialist, Online Marketing professional, Freelancers and anyone.</p>
            <div class="row">
              <div class="col-lg-3">
                <img src="bebas/User-Pic.svg" alt="">
              </div>
                <div class="col-lg-8 pt-2 ml-2">
                  <h5>Arthur Broklyn</h5>
                  <p>Categories: 3d Modelling</p>
                </div>      
            </div>
          </div>
        </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="container">
            <img src="bebas/testi.svg" alt=""  >
            <p class="card-text pt-3 pb-3">Semaj Africa is an online education platfrom that delivers video courses, program and resources for Individual, Advertising & Media Specialist, Online Marketing professional, Freelancers and anyone.</p>
            <div class="row">
              <div class="col-lg-3">
                <img src="bebas/User-Pic.svg" alt="">
              </div>
                <div class="col-lg-8 pt-2 ml-2">
                  <h5>Arthur Broklyn</h5>
                  <p>Categories: 3d Modelling</p>
                </div>      
            </div>
          </div>
        </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="container">
            <img src="bebas/testi.svg" alt=""  >
            <p class="card-text pt-3 pb-3">Semaj Africa is an online education platfrom that delivers video courses, program and resources for Individual, Advertising & Media Specialist, Online Marketing professional, Freelancers and anyone.</p>
            <div class="row">
              <div class="col-lg-3">
                <img src="bebas/User-Pic.svg" alt="">
              </div>
                <div class="col-lg-8 pt-2 ml-2">
                  <h5>Arthur Broklyn</h5>
                  <p>Categories: 3d Modelling</p>
                </div>      
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
</section>
<!-- Akhir Testimonials -->

<!-- Newsletter -->
<section class="newsletter d-flex align-items-center" id="newsletter">
  <div class="container">
    <img src="bebas/Rectangle7.svg" alt="" class="imgs">
    <div class="row justify-content-center mt-5">
      <div class="col-lg-8">
        <h4 class="display-h4">Subscription to Our Newsletter</h4>
        <p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
        <button class="btn btn-primary">Start free trial</button>
      </div>
    </div>
  </div>
</section>
<!-- Akhir Newsletter -->

<!-- Contact -->
<section class="contact d-flex align-items-center" id="contact">
  <div class="row justify-content-center">
    <div class="col-lg-3">
      <img src="bebas/Logo.svg" alt="" class="mb-3">
      <p>Semaj Africa is an online education platfrom that delivers video courses, program and resources</p>
      <img src="bebas/sosmed.svg" alt="" class="mt-5">
    </div>
    <div class="col-lg-2">
      <h6>Quicklinks</h6>
      <ul>
        <li>Home</li>
        <li>Courses</li>
        <li>About Us</li>
        <li>Contact Us</li>
      </ul>
    </div>
    <div class="col-lg-3">
      <h6>Contact Us</h6>
      <ul>
        <li>(+55) 254. 254. 254</li>
        <li>info@lsemajafrica.com</li>
        <li>Helios Tower 75 Tam Trinh Hoang</li>
        <li>Mai - Ha Noi - Viet Nam</li>
      </ul>
    </div>
    <div class="col-lg-3">
      <h6>Terms and Conditions Faq</h6>
    </div>
  </div>
</section>
<!-- Akhir Contact -->
</body>
</html>